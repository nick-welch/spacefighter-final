
#include <string>
#include "MainMenuScreen.h"
#include "GameplayScreen.h"
#include "GameOverScreen.h"


// Callback Functions
void OnRetrySelect(MenuScreen* pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen());
}

void RetryOnQuitSelect(MenuScreen* pScreen)
{
	GameOverScreen* pGameOverScreen = (GameOverScreen*)pScreen;
	pGameOverScreen->SetQuitFlag();
	pGameOverScreen->Exit();
}

void RetryOnScreenRemove(Screen* pScreen)
{
	GameOverScreen* pGameOverScreen = (GameOverScreen*)pScreen;
	if (pGameOverScreen->IsQuittingGame()) pScreen->GetGame()->Quit();
}



GameOverScreen::GameOverScreen()
{
	m_pTexture = nullptr;

	SetRemoveCallback(RetryOnScreenRemove);

	SetTransitionInTime(1.0f);
	SetTransitionOutTime(0.5f);

	Show(); // Show the screen
}

void GameOverScreen::LoadContent(ResourceManager* pResourceManager)
{
	// Logo
	m_pTexture = pResourceManager->Load<Texture>("Textures\\GameOver.png");
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 150;

	// Create the menu items
	const int COUNT = 2;
	MenuItem* pItem;
	Font::SetLoadSize(70, true);
	Font* pFont = pResourceManager->Load<Font>("Fonts\\retry.ttf");
	//Font* pFont = pResourceManager->Load<Font>("Fonts\\retry.ttf");

	SetDisplayCount(COUNT);

	enum Items { RETRY, QUIT };
	std::string text[COUNT] = { "Retry", "Quit" };

	for (int i = 0; i < COUNT; i++)
	{
		pItem = new MenuItem(text[i]);
		pItem->SetPosition(Vector2(450, 500 + 100 * i));
		pItem->SetFont(pFont);
		pItem->SetColor(Color::Blue);
		pItem->SetSelected(i == 0);
		AddMenuItem(pItem);
	}

	GetMenuItem(RETRY)->SetSelectCallback(OnRetrySelect);
	GetMenuItem(QUIT)->SetSelectCallback(RetryOnQuitSelect);
}

void GameOverScreen::Update(const GameTime* pGameTime)
{
	MenuItem* pItem;

	// Set the menu item colors
	for (int i = 0; i < GetDisplayCount(); i++)
	{
		pItem = GetMenuItem(i);
		pItem->SetAlpha(GetAlpha());

		if (pItem->IsSelected()) pItem->SetColor(Color::White);
		else pItem->SetColor(Color::DarkRed);
	}

	MenuScreen::Update(pGameTime);
}

void GameOverScreen::Draw(SpriteBatch* pSpriteBatch)
{
	pSpriteBatch->Begin();
	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter());
	pSpriteBatch->End();

	MenuScreen::Draw(pSpriteBatch);
}