
#include "GameplayScreen.h"
#include "Level.h"
#include "Level01.h"
#include "Level02.h"
#include <windows.h>
#pragma comment(lib,"Winmm.lib")

using namespace KatanaEngine;

GameplayScreen::GameplayScreen(const int levelIndex)
{
	m_pLevel = LoadLevel(levelIndex);

	SetTransitionInTime(1.0f);
	SetTransitionOutTime(0.5f);
	//sndPlaySound("..\\SpaceFighter\\Content\\Music\\Level01.wav", SND_FILENAME | SND_ASYNC | SND_LOOP);
	Show();
}

Level *GameplayScreen::LoadLevel(const int levelIndex)
{
	Level *pLevel = nullptr;

	switch (levelIndex)
	{
	case 0: pLevel = new Level01(); break;
	case 1: pLevel = new Level02(); break;
	}

	ScreenManager *pSM = GetScreenManager();
	if (pLevel && pSM) pLevel->LoadContent(pSM->GetResourceManager());
	
	return pLevel;
}

void GameplayScreen::LoadContent(ResourceManager *pResourceManager)
{
	m_pLevel->LoadContent(pResourceManager);
}

void GameplayScreen::HandleInput(const InputState *pInput)
{
	m_pLevel->HandleInput(pInput);
}

void GameplayScreen::Update(const GameTime *pGameTime)
{
		m_pLevel->Update(pGameTime);
	/**/
	if (m_pLevel->IsComplete())
	{
		int levelToLoad = m_pLevel->GetLevelToLoad();
		delete m_pLevel;
		m_pLevel = LoadLevel(levelToLoad);
	}
	/**/
}


void GameplayScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();

	m_pLevel->Draw(pSpriteBatch);

	pSpriteBatch->End();
	//test
}

void GameplayScreen::DisplayScore()
{
	//Add something here to make it display on the screen


	//std::cout << "Score: " << EnemyShip::GetScore() << std::endl;
}