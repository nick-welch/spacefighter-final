

#include "Level01.h"
#include "BioEnemyShip.h"
#include <allegro5\allegro_audio.h>
#include <allegro5\allegro_acodec.h>



BioEnemyShip* pEnemy;

void Level01::LoadContent(ResourceManager *pResourceManager)
{
	al_stop_samples();
	
	m_pSong = al_load_sample("..\\SpaceFighter\\Content\\Music\\Level01.wav");
	
	al_play_sample(m_pSong,1,0.0,1.0,ALLEGRO_PLAYMODE_LOOP,0);
	


	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");
	
	
	const int COUNT = 21;

	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};
	
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};

	float delay = 3.0; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->SetLastEnemy(false);
		pEnemy->Initialize(position, (float)delay);

		if (i == 20) 
		{
			pEnemy->SetLastEnemy(true);
		}
			

		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);
}

bool Level01::IsComplete()
{
	
		if (Level::GetComplete()) {

			al_stop_samples();
			return true;
			
		}
	
	
	return false;
}

int Level01::GetLevelToLoad()
{
	return 1;
}