
#include "EnemyShip.h"
#include "GameplayScreen.h"
#include "Level02.h"


int EnemyShip::m_ShipsDestroyed = 0;
int EnemyShip::m_Score = 0;

EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}

	if (IsActive())
	{
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}

int EnemyShip::GetShipsDestroyed()
{
	return EnemyShip::m_ShipsDestroyed;
}

void EnemyShip::SetShipsDestroyed(int shipsDestroyed)
{
	EnemyShip::m_ShipsDestroyed = shipsDestroyed;
}


void EnemyShip::Hit(const float damage)
{
	//adds damage
	Ship::Hit(damage);

	//Increment score counter
	m_Score++;

	//Increment the counter of number of ships destroyed
	int shipsDestroyed = GetShipsDestroyed();
	shipsDestroyed++;
	SetShipsDestroyed(shipsDestroyed);

	//if all ships are destroyed, go to level 2
	
}