

#include "Level02.h"
#include "BioEnemyShip.h"
#include "GameplayScreen.h"

BioEnemyShip *pEnemy02;

void Level02::LoadContent(ResourceManager* pResourceManager)
{
	
	m_pSong = al_load_sample("..\\SpaceFighter\\Content\\Music\\daylight.wav");
	al_play_sample(m_pSong, 1, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, 0);
	// Setup enemy ships
	Texture* pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	const int COUNT = 40;

	double xPositions[COUNT] =
	{
		0.2, 0.2, 0.2, 0.8, 0.5,
		0.75, 0.8, 0.7, 0.1, 0.9, 0.5,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.5, 0.5, 0.5, 0.5, 0.5,
		0.5, 0.4, 0.6, 0.45, 0.55,
		0.3, 0.25, 0.35, 0.2,
		0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95
	};

	double delays[COUNT] =
	{
		0.0, 0.25, 0.25, 0.25, 0.25,
		3.0, 0.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3,
		3.5, 0.3, 0.3, 0.3,
		2.0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1
	};

	float delay = 3.0; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		pEnemy02 = new BioEnemyShip();
		pEnemy02->SetTexture(pTexture);
		pEnemy02->SetCurrentLevel(this);
		pEnemy02->Initialize(position, (float)delay);
		

		if (i == COUNT - 1)
		{
			pEnemy02->SetLastEnemy(true);
		}
			

		AddGameObject(pEnemy02);
	}

	Level::LoadContent(pResourceManager);
}

bool Level02::IsComplete()
{
	
	if (Level::GetComplete()) {
		al_stop_samples();
		return true;
	}

	return false;


}

int Level02::GetLevelToLoad() 
{
	return 0;
}